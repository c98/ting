﻿using System;
using UnityEngine;

namespace Game.Audio
{

    public enum SystemAudioState
    {
        Background,
        InitialSpawn
    }
    
    public class AudioManager: MonoBehaviour
    {
        private SystemAudioState currentSAudioType;
        [Header("Audio")]
        public AudioSource BackgroundMusic;
        public AudioSource SpawnSound;
        [SerializeField] Boolean isMuted;
        
        void Awake()
        {
            isMuted = false;
            BackgroundMusic = GetComponent<AudioSource>();
            SpawnSound = GetComponent<AudioSource>();
        }

        public SystemAudioState CurrentState
        {
            get { return currentSAudioType; }
            set { currentSAudioType = value; }
        }

        public void PlayBackGroundMusic()
        {
            if (isMuted){ return;}
            BackgroundMusic.Play();
        }
        
        public void PlayInitialSound()
        {
            if (isMuted){ return;}
            SpawnSound.Play();
        }
   
        
    }
}