﻿using System;
using UnityEngine;

namespace Game.Audio
{
    
    public enum EnemiesSoundState
    {
        Scared,
        Shooting,
        Dead
    }
    
    public class EnemyAudioManager: MonoBehaviour
    {
        private EnemiesSoundState currentenemiesSoundType;
        public AudioSource ScaredSound;
        public AudioSource ShootingSound;
        public AudioSource DeadSound;
        [SerializeField] Boolean isMuted;

                
        void Awake()
        {
            isMuted = false;
        }

        public EnemiesSoundState Current
        {
            get { return currentenemiesSoundType; }
            set { currentenemiesSoundType = value; }
        }
        
          
    }
}