﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum MonsterState
{
	idle = 0,
	Attack = 1,
	Walk = 2
}

public class MonsterAnimationManager : MonoBehaviour
{

	public Animator MonsterAnimattor;
	public MonsterState currentState;
	private Boolean IsSearchingPlayer;
	
	//Audio
	public AudioSource AppearSound;
	
	// Use this for initialization
	void Start ()
	{
		IsSearchingPlayer = false;
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public MonsterState CurrentState
	{
		get { return currentState; }
		set
		{
			currentState = value;
			switch (currentState)
			{
					case MonsterState.idle:
						MonsterAnimattor.SetTrigger(MonsterState.idle.ToString());
						break;
					case MonsterState.Attack:
						MonsterAnimattor.SetTrigger(MonsterState.Attack.ToString());
						break;
					case MonsterState.Walk:
						MonsterAnimattor.SetTrigger(MonsterState.Walk.ToString());
						break;
			}
		}
	}

	public void Walking()
	{
		MonsterAnimattor.SetTrigger(MonsterState.Walk.ToString());
	}
	public void Attacking()
	{
		MonsterAnimattor.SetTrigger(MonsterState.Attack.ToString());
	}
}
