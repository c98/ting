﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CageControlGUI : MonoBehaviour
{

    public UnityEngine.Animation CageAnimation;
    private GLTF.Schema.Animation animator;
    // Use this for initialization
    void Start ()
    {
        CageAnimation = GetComponent<UnityEngine.Animation>();
    }
	
    // Update is called once per frame
    private void OnGUI()
    {
        if (GUI.Button(new Rect(Screen.width - 480, 20, 140, 40), "Open"))
        {
            //animator.Play("open_door");
            CageAnimation.Play();

        }
        if (GUI.Button(new Rect(Screen.width - 480, 60, 140, 40), "Close"))
        {
           // animator.Play("close_door");
        }

    }
}
