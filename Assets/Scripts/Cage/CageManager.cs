﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class CageManager:MonoBehaviour
{
    public List<GameObject> ObjectInCages = new List<GameObject>();
    private int cageCount;
    public GameObject cagePrefabs;

    private float x;
    private float y;
    
    private void Awake()
    {
        x = Random.Range(-5,5);
        y = Random.Range(-5,5);
        cageCount = 5;
        GeneratorCage();
    }

    public void GeneratorCage()
    {

        for (int i = 0; i < cageCount; i++)
        {
          var cages = Instantiate(cagePrefabs, new Vector3(i * 2.0F, 0, 0), Quaternion.identity);
           ObjectInCages.Add(cages);
        }
        Debug.Log(ObjectInCages.Count);
    }
}
