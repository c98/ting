﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.WSA.Input;

namespace Animation
{

	public enum CageState
	{
		Open,
		Close
	}

	public class CageAnimator : MonoBehaviour
	{
		[Header ("Animations")]
		public UnityEngine.Animation CgeAnimator;
		public CageState currentState;
		public CageManager ObjectInFocus;
		public Boolean IsTaping;
		public Boolean IsDetected;

		
		private void Awake()
		{
			//currentState = CateState.Run;
			GestureRecognizer gestureRecognizer = new GestureRecognizer();
			gestureRecognizer.SetRecognizableGestures(UnityEngine.XR.WSA.Input.GestureSettings.Tap);
			gestureRecognizer.TappedEvent += GestureRecognizer_TappedEvent;
			gestureRecognizer.StartCapturingGestures();
			
			CgeAnimator = GetComponent<UnityEngine.Animation>();
			IsTaping = false;
			IsDetected = false;
			//CgeAnimator = GetComponent<Animator>();
			ObjectInFocus = GetComponent<CageManager>();
			
		}

		private void GestureRecognizer_TappedEvent(InteractionSourceKind source, int tapcount, Ray headray)
		{
			Debug.Log("not Gestured Detected");
			if (ObjectInFocus.ObjectInCages == null)
				return;
			if (IsDetected || IsTaping == false)
			{
				IsTaping = true;
			}
			OpenCage();
			Debug.Log("Gestured Detected");
		}
		
		public void OpenCage()
		{
			//CgeAnimator.Play("open_door");
			CgeAnimator.Play();
		}

		public CageState CurrentState
		{
			get {return currentState;}
			set
			{
				currentState = value;
				switch (currentState)
				{
					case CageState.Open:
						//CgeAnimator.SetTrigger(CageState.Open.ToString());
						GetComponent<Animator>().Play("open_door");
						break;
					case CageState.Close:		
						//CgeAnimator.SetTrigger(CageState.Close.ToString());
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
				
			}
		}
		
		// Update is called once per frame
		void Update()
		{
			HitDetected();
		}

		public void HitDetected()
		{
			var ray = new Ray(Camera.main.transform.position, Camera.main.transform.forward);
			RaycastHit raycastInfo;
			if (Physics.Raycast(ray, out raycastInfo))
			{
				IsDetected = true;
				//if (IsTaping)
			//{
					CgeAnimator.Play();
					IsTaping = false;
					
				//}
			}
			else
			{
				
			}
		}
		
		public void CageAnimation()
		{
			CgeAnimator.Play("open_door");
		}
	}

}
