﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

public class ForHealthBar : MonoBehaviour
{
	private Transform bar;
	private Color color;
	public float health = 1f;
	// Use this for initialization
	void Start ()
	{
		bar = transform.Find("Bar");
		//sizeNormalizeda to adjust value
	}
	
	// Update is called once per frame
	void Update ()
	{
		//Set Value
		health -= 0.001f;
		SetSize(health);
		//Set Color
		print(health);

		if (health <= 0.4f)
		{
			if (Mathf.Floor((health * 100f) % 3 ) == 0)
			{
				color = Color.white;
			}
			else
			{
				color = Color.red;
			}
			SetColor(color);
		}
	}

	public void SetSize(float sizeNormalized)
	{
		if (health <= 0f)
		{
			sizeNormalized = 0.0f;
		}
		bar.localScale = new Vector2(sizeNormalized,1f);
	}

	public void SetColor(Color color)
	{
		bar.Find("BarSprite").GetComponent<SpriteRenderer>().color = color;
	}
}
