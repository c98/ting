﻿using System;
using UnityEngine;

namespace Animation
{

    public enum CateState
    {
        Idle,
        Move,
        Run,
        Jump
    }
    
    public class CatAnimation:MonoBehaviour
    {
        [Header ("Animations")]
        public Animator CatAnimator;
        public CateState currentState;

        public Boolean IsCatching;
        
        //Audio
        public AudioSource runningSound;


        [Header("idle-Time")]
        private float idleTime;
        
        private void Awake()
        {
            //currentState = CateState.Run;
            CatAnimator = GetComponent<Animator>();
        }
        
        public CateState CurrentState
        {
            //inside field value to save 
            get { return currentState; }
            set
            {
                currentState = value;
                switch (currentState)
                {
                    case CateState.Idle:
                        CatAnimator.SetTrigger(CateState.Idle.ToString()); 
                        break;
                    case CateState.Move:
                        CatAnimator.SetTrigger(CateState.Move.ToString()); 
                        break;
                    case CateState.Run:
                        CatAnimator.SetTrigger(CateState.Run.ToString()); 
                        break;
                    case CateState.Jump:
                        CatAnimator.SetTrigger(CateState.Jump.ToString());
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public void AnimateCat()
        {
            CatAnimator.SetTrigger(CateState.Jump.ToString());
        }

    }
    
    
}